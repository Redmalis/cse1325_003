#include "stdafx.h"
#include "Robot.h"



//////////////
//RobotModel//
//////////////

RobotModel::RobotModel(vector<string> incModelInformation) {
	if (incModelInformation.size() > 0) setModelName(incModelInformation[0]);
	if (incModelInformation.size() > 1) setModelNumber(atoi(incModelInformation[1].c_str()));
	if (incModelInformation.size() > 2) setRobotMarkup(atof(incModelInformation[2].c_str()));	
}

string RobotModel::getModelString(char screenOrFile) {
	string tempSeparator=""; 
	if (screenOrFile=='f') tempSeparator = "|";else tempSeparator = "\t";
	string tempString = getModelName() + tempSeparator + to_string(getModelNumber())+ tempSeparator +to_string(getRobotMarkup());
	for (int partsIterator = 0; partsIterator < robotParts.size(); partsIterator++) tempString = tempString + tempSeparator + to_string(robotParts[partsIterator]->getPartNumber());
	return tempString;
}

string RobotModel::getModelName() {
	return modelName;
}

void RobotModel::setModelName(string incName) {
	modelName = incName;
}

int RobotModel::getModelNumber() {
	return modelNumber;
}

void RobotModel::setModelNumber(int incNumber) {
	modelNumber = incNumber;
}

double RobotModel::getRobotWeight() {
	double tempWeight = 0;
	
	for (int wIterator = 0; wIterator <(int)robotParts.size(); wIterator++) {
		tempWeight += robotParts[wIterator]->getPartWeight();
	}
	
	return tempWeight;
}

double RobotModel::calculatePartsTotalCost() {
	double tempCost = 0;
	for (int costIterator = 0; costIterator < (int)robotParts.size(); costIterator++) {
		tempCost += robotParts[costIterator]->getPartCost();
	}
	return tempCost;
}

void RobotModel::setRobotMarkup(double incMarkup) {
	robotMarkUp = incMarkup;
}

double RobotModel::getRobotMarkup() {
	return robotMarkUp;
}

double RobotModel::getRobotPrice() {
	return calculatePartsTotalCost()+getRobotMarkup();
}

void RobotModel::addRobotPart(RobotPart* newPart) {
 
	int partsCount = 0;
	for(int partsIterator = 0;partsIterator<(int)robotParts.size();partsIterator++){
		if (robotParts[partsIterator]->getPartTypeInt() == newPart->getPartTypeInt()) partsCount++;
	}
	if (partsCount < partTypeMaxes[newPart->getPartTypeInt()]) {
		robotParts.push_back(newPart);
		if (newPart->getPartTypeInt() == 2) {
			Torso * tempPart = (Torso*)newPart;
			partTypeMaxes[3] = tempPart->getMaxBatterCompartments();
			setHasTorso();
		}
	}
	else throw exception("Already have max of that part type.");
}

void RobotModel::setHasTorso() {
	hasTorso = 1;
}

int RobotModel::getHasTorso() {
	return hasTorso;
}

/////////////
//PartType//
////////////

PartType::PartType() {
	typeVal = 0;
}

PartType::PartType(int incTypeVal) {
	typeVal = incTypeVal;
}

string PartType::to_string(){
 switch (typeVal) {
	case(head): return "Head";
	case(arm): return "Arm";
	case(torso): return "Torso";
	case(battery): return "Battery";
	case(locomotor): return "Locomotor";
	default: return "Invalid Part";
	}
}

int PartType::to_int() {
	return typeVal;
}



/////////////
//RobotPart//
/////////////

RobotPart::RobotPart() {}

string RobotPart::getPartName() {
	return partName;
}

void RobotPart::setPartName(string incName) {
	partName = incName;
}

int RobotPart::getPartNumber() {
	return partNumber;
}

void RobotPart::setPartNumber(int incNumber) {
	partNumber = incNumber;
}

double RobotPart::getPartWeight() {
	return partWeight;
}

void RobotPart::setPartWeight(double incWeight) {
	partWeight = incWeight;
}

double RobotPart::getPartCost() {
	return partCost;
}

void RobotPart::setPartCost(double incCost) {
	partCost = incCost;
}

string RobotPart::getPartDescription() {
	return partDescription;
}

void RobotPart::setPartDescription(string incDescription) {
	partDescription = incDescription;
}

string RobotPart::getPartTypeStr() {
	return partType.to_string();
}

int RobotPart::getPartTypeInt() {
	return partType.to_int();
}

void RobotPart::setPartType(int incType) {
	partType = PartType(incType);
}

string RobotPart::basePartString(char screenOrFile) {
	string tempString = "";
	if (screenOrFile == 'f')
		tempString += getPartName() + "|" + to_string(getPartTypeInt())+"|"+ to_string(getPartNumber()) + "|" + getPartDescription();
	else
		tempString += getPartName() + "\t" + to_string(getPartTypeInt()) + "\t" + to_string(getPartNumber()) + "\n\t" + getPartDescription();
	return tempString;
}
string RobotPart::getPartStringLong(char screenOrFile) {
	if(screenOrFile=='f')
	return basePartString(screenOrFile)+ "|" + to_string(getPartWeight()) + "|" + to_string(getPartCost()) ;
	else
		return basePartString(screenOrFile) + "\t" + to_string(getPartWeight()) + "\t" + to_string(getPartCost());
}

string RobotPart::getPartStringShort(char screenOrFile) {
	return basePartString(screenOrFile);
}

string RobotPart::getPartString(int shortOrLong, char screenOrFile) {
	if (shortOrLong == 1) return getPartStringLong(screenOrFile);
	else return getPartStringShort(screenOrFile);
}

//Image RobotPart::getPartImage(){}
//void RobotPart::setPartImage(Image){}

////////
//Head//
////////

Head::Head(vector<string> incPartInformation) {
	setPartName(incPartInformation[0]);
	setPartNumber(atoi(incPartInformation[2].c_str()));
	setPartDescription(incPartInformation[3]);
	setPartWeight(atoi(incPartInformation[4].c_str()));
	setPartCost(atoi(incPartInformation[5].c_str()));
	setPartType(0);
}


///////
//Arm//
///////

Arm::Arm(vector<string> incPartInformation) {
	setPartName(incPartInformation[0]);
	setPartNumber(atoi(incPartInformation[2].c_str()));
	setPartDescription(incPartInformation[3]);
	setPartWeight(atoi(incPartInformation[4].c_str()));
	setPartCost(atoi(incPartInformation[5].c_str()));
	setPowerConsumption(atoi(incPartInformation[6].c_str()));
	setPartType(1);
}

void Arm::setPowerConsumption(int incConsumption) {
	powerConsumption = incConsumption;
}

int Arm::getPowerConsumption() {
	return powerConsumption;
}

string Arm::getPartStringLong(char screenOrFile) {
	if (screenOrFile == 'f')
	return basePartString(screenOrFile) + "|" + to_string(getPartWeight()) + "|" + to_string(getPartCost()) +  "|" + to_string(getPowerConsumption());
	else
	return basePartString(screenOrFile) + "\t" + to_string(getPartWeight()) + "\t" + to_string(getPartCost()) + "\t" + to_string(getPowerConsumption());
}

string Arm::getPartString(int shortOrLong, char screenOrFile) {
	if (shortOrLong == 1) return getPartStringLong(screenOrFile);
	else return getPartStringShort(screenOrFile);
}


///////////
//Battery//
///////////

Battery::Battery(vector<string> incPartInformation) {
	setPartName(incPartInformation[0]);
	setPartNumber(atoi(incPartInformation[2].c_str()));
	setPartDescription(incPartInformation[3]);
	setPartWeight(atoi(incPartInformation[4].c_str()));
	setPartCost(atoi(incPartInformation[5].c_str()));
	setEnergy(atoi(incPartInformation[6].c_str()));
	setPartType(3);
}

void Battery::setEnergy(double incEnergy) {
	energy = incEnergy;
}

double Battery::getEnergy() {
	return energy;
}

string Battery::getPartStringLong(char screenOrFile) {
	if (screenOrFile == 'f')
	return basePartString(screenOrFile)+ "|" + to_string(getPartWeight()) + "|" + to_string(getPartCost()) +  "|" + to_string(getEnergy());
	else
		return basePartString(screenOrFile) + "\t" + to_string(getPartWeight()) + "\t" + to_string(getPartCost())  + "\t" + to_string(getEnergy());
}

string Battery::getPartString(int shortOrLong, char screenOrFile) {
	if (shortOrLong == 1) return getPartStringLong(screenOrFile);
	else return getPartStringShort(screenOrFile);
}


/////////
//Torso//
/////////

Torso::Torso(vector<string> incPartInformation) {
	setPartName(incPartInformation[0]);
	setPartNumber(atoi(incPartInformation[2].c_str()));
	setPartDescription(incPartInformation[3]);
	setPartWeight(atoi(incPartInformation[4].c_str()));
	setPartCost(atoi(incPartInformation[5].c_str()));
	setMaxBatteryCompartments(atoi(incPartInformation[6].c_str()));
	setPartType(2);
}

void Torso::setMaxBatteryCompartments(int incMax) {
	maxBatteryCompartments = incMax;
}

int Torso::getMaxBatterCompartments() {
	return maxBatteryCompartments;
}

void Torso::insertBattery(RobotPart& incBattery) {
	if (maxBatteryCompartments > 0 && (int)batteries.size() < maxBatteryCompartments) {
		batteries.push_back(&incBattery);
	}
	else throw exception("Battery compartments already full."); 
}

string Torso::getPartStringLong(char screenOrFile) {
	if(screenOrFile=='f')
		return basePartString(screenOrFile) + "|" + to_string(getPartWeight()) + "|" + to_string(getPartCost()) + "|" + to_string(getMaxBatterCompartments());
	else
		return basePartString(screenOrFile) + "\t" + to_string(getPartWeight()) + "\t" + to_string(getPartCost()) + "\t" + to_string(getMaxBatterCompartments());
}

string Torso::getPartString(int shortOrLong, char screenOrFile) {
	if (shortOrLong == 1) return getPartStringLong(screenOrFile);
	else return getPartStringShort(screenOrFile);
}

/////////////
//Locomotor//
/////////////

Locomotor::Locomotor(vector<string> incPartInformation) {
	setPartName(incPartInformation[0]);
	setPartNumber(atoi(incPartInformation[2].c_str()));
	setPartDescription(incPartInformation[3]);
	setPartWeight(atoi(incPartInformation[4].c_str()));
	setPartCost(atoi(incPartInformation[5].c_str()));
	setMaxSpeed(atoi(incPartInformation[6].c_str()));
	setPartType(4);
}

void Locomotor::setMaxSpeed(int incMaxSpeed) {
	maxSpeed = incMaxSpeed;
}

int Locomotor::Locomotor::getMaxSpeed() {
	return maxSpeed;
}

string Locomotor::getPartStringLong(char screenOrFile) {
	if(screenOrFile=='f')
	return basePartString(screenOrFile) + "|" + to_string(getPartWeight()) + "|" + to_string(getPartCost()) + "|" + to_string(getMaxSpeed());
	else
	return basePartString(screenOrFile) + "\t" + to_string(getPartWeight()) + "\t" + to_string(getPartCost()) + "\t" + to_string(getMaxSpeed());
}

string Locomotor::getPartString(int shortOrLong, char screenOrFile) {
	if (shortOrLong == 1) return getPartStringLong(screenOrFile);
	else return getPartStringShort(screenOrFile);
}


