#ifndef __Order_H
#define __Order_H 2016

#include "Date.h"


class Order {
public:
	enum Status
	{
		Ordered,InAssembly,Assembled,PreShipping,Shipped,Complement
	};
	int getOrderNumber();
	void setOrderNumber(int incOrderNumber);
	double getRobotPrice();
	double calculateShippingCost();
	double calculateOrderTax(double incOrderPreTotal);
	double setTotalOrderPrice(double incPriceSum);
	void setCustomer(Customer* incCustomer);
	Customer* getCustomer();
	void setSalesAssociate(SalesAssociate* incAssociate);
	SalesAssociate* getSalesAssociate();
	void setRobotModel(RobotModel* incRobot);
	RobotModel* getRobotModel();
	void setStatus(int incStatus);
	int getStatus();
	//void setDate(string dateStr);
	//string getDate();

private:

	int orderNumber;
	double totalOrderPrice;
	Date orderDate;
	Customer* customer;
	SalesAssociate* salesAssociate;
	RobotModel* robot;
	Status orderStatus;
};
#endif
