#include "stdafx.h"
#include "People.h"

//////////
//Person//
//////////

string Person::getName() {
	return name;
}

void Person::setName(string incName) {
	name = incName;
}

/////////////
//Customer//
////////////

int Customer::getCustomerNumber() {
	return customerNumber;
}

void Customer::setCustomerNumber(int incNumber) {
	customerNumber = incNumber;
}

double Customer::getWallet() {
	return wallet;
}

void Customer::setWallet(double incWallet) {
	wallet = incWallet;
}

/*
void Customer::addOrder(Order incOrder) {
	orders.push_back(incOrder);
}

vector<Order> Customer::retrieveOrders() {
	return orders;
}
*/

//////////////////
//SalesAssociate//
//////////////////

int SalesAssociate::getEmployeeNumber() {
	return employeeNumber;
}

void SalesAssociate::setEmployeeNumber(int incNumber) {
	employeeNumber = incNumber;
}

/*
void SalesAssociate::addOrder(Order incOrder) {
	orders.push_back(incOrder);
}

vector<Order> SalesAssociate::retrieveOrders() {
	return orders;

}
*/

//////////////////
//ProductManager//
//////////////////


int ProductManager::getEmployeeNumber() {
	return employeeNumber;
}

void ProductManager::setEmployeeNumber(int incNumber) {
	employeeNumber = incNumber;
}

string ProductManager::getString(char screenOrFile) {
	string tempSeparator = "";
	if (screenOrFile == 'f') tempSeparator = "|"; else tempSeparator = "\t";
	string tempString = getName() + tempSeparator + to_string(getEmployeeNumber());
		return tempString;
}

/*
void ProductManager::addRobotModel(RobotModel incRobot) {
	robots.push_back(incRobot);
}

vector<RobotModel> ProductManager::retrieveRobotModels() {
	return robots;
}
*/