#ifndef __windowedUI_H
#define __windowedUI_H 2016
#include <FL/Fl_Widget.H>



class menuWindow: public RobotShop{
public:
	static void CloseCB(Fl_Widget* callWidget, RobotShop * incShop);
	static void mainCB(Fl_Widget * callWidget, RobotShop * incShop);
	RobotShop * currentShop;
};

class UserWindow: public menuWindow {
public:
	Fl_Window  * window;
	Fl_Group * menuGroup;
	Fl_Button * custButton;
	Fl_Button * emplButton;

	UserWindow(RobotShop * incShop);
	static void custEntryCB(Fl_Widget * callWidget, RobotShop * incShop);
	static void emplEntryCB(Fl_Widget * callWidget, RobotShop * incShop);
};


class EmplWindow: public menuWindow {
public:
	Fl_Window * window;
	Fl_Group * menuGroup;
	Fl_Button * saButton;
	Fl_Button * pmButton;
	Fl_Button * phbButton;
	
	EmplWindow(RobotShop * incShop);
	static void saEntryCB(Fl_Widget * callWidget, RobotShop * incShop);
	static void pmEntryCB(Fl_Widget * callWidget, RobotShop * incShop);
	static void phbEntryCB(Fl_Widget * callWidget, RobotShop * incShop);
	
};

class userMenuWindow: public menuWindow{
public:	
	const static int maxMenu = 12;
	Fl_Window * window;
	Fl_Menu_Bar * menuBar;
    //Fl_Menu_Item menuItems[maxMenu];

	static void createPartCB(Fl_Widget * callWidget, RobotShop * incShop);
	static void createModelCB(Fl_Widget * callWidget, RobotShop * incShop);
	static void viewPartCB(Fl_Widget * callWidget, RobotShop * incShop);
	static void viewModelCB(Fl_Widget * callWidget, RobotShop * incShop);
};

class saMenuWindow : public userMenuWindow {
 public:
	saMenuWindow(RobotShop * incShop);
};

class pmMenuWindow : public userMenuWindow {
 public:	
	pmMenuWindow(RobotShop * incShop);
}; 

class phbMenuWindow : public userMenuWindow {
 public:
	phbMenuWindow(RobotShop * incShop);
};

class custMenuWindow : public userMenuWindow {
 public:
	custMenuWindow(RobotShop * incShop);
};

class reportWindow : public menuWindow, public UserInterface {
public:	
	static void modelInfoCB(Fl_Widget * callWidget, RobotModel * incModel);
	static void partInfoCB(Fl_Widget * callWidget, RobotPart * incPart);
};

class moreInfoWindow : public reportWindow {
public:
	moreInfoWindow(RobotModel* incModel);
	moreInfoWindow(RobotPart* incPart);
};

class reportModelsWindow : public reportWindow {
public:
	Fl_Window * window;
	Fl_Button * okButton;
	Fl_Group * partsDisplay;
	reportModelsWindow(RobotShop * incShop);
	
};

class reportPartsWindow: public reportWindow{
public:
	Fl_Window * window;
	Fl_Button * okButton;
	Fl_Group * partsDisplay;
	reportPartsWindow(RobotShop * incShop);

};

class partChoiceWindow : public menuWindow {
public:
	partChoiceWindow(RobotShop * incShop);
	Fl_Window * window;
	Fl_Box * instruct;
	Fl_Choice * partType;
	Fl_Button * canButton;

    static void typeChosenCB(Fl_Widget * callWidget, RobotShop * incShop);
};

class newPartWindow : public menuWindow {
public:
	struct partData
	{
		string name;
		string type;
		string number;
		string desc;
		string weight;
		string cost;
		string spec;
	};
	struct subData {
		struct partData * subDatum;
		RobotShop * subShop;
	};
	Fl_Window * window;
	Fl_Input *nameInput;
	Fl_Output *partNumber;
	Fl_Input *weightInput;
	Fl_Input *costInput;
	Fl_Input * descInput;
	Fl_Button * subButton;
	Fl_Button * clrButton;
	Fl_Button * canButton;
	struct partData * datum;

	newPartWindow(RobotShop * incShop);
	newPartWindow() : newPartWindow(NULL) {};
	static void subPartCB(Fl_Widget * callWidget, struct subData * incSubData);
	static void setNameCB(Fl_Widget * callWidget, struct partData * locDatum);
	static void setNumberCB(Fl_Widget * callWidget, struct partData * locDatum);
	static void setWeightCB(Fl_Widget * callWidget, struct partData * locDatum);
	static void setCostCB(Fl_Widget * callWidget, struct partData * locDatum);
	static void setDescCB(Fl_Widget * callWidget, struct partData * locDatum);
	static void setSpecCB(Fl_Widget * callWidget, struct partData * locDatum);
};

class newHeadWindow : public newPartWindow {
public:
	newHeadWindow(RobotShop * incShop);
};
class newArmWindow: public newPartWindow{
public:
	Fl_Input * armInput;
		newArmWindow(RobotShop * incShop);
};
class newTorsoWindow : public newPartWindow {
public:
	Fl_Input * torsoInput;
	newTorsoWindow(RobotShop * incShop);
};
class newBatteryWindow : public newPartWindow {
public:
	Fl_Input * batteryInput;
	newBatteryWindow(RobotShop * incShop);
};
class newLocomWindow : public newPartWindow{
public:
	Fl_Input * locomInput;
	newLocomWindow(RobotShop * incShop);
};

class newModelWindow : public menuWindow {
public:
	struct modelData
	{
		string name;
		string number;
		string weight;
		string partsCost;
		string markup;
		string hasTorso;
		vector<string> robotPartNums;
	};
	struct subData {
		struct modelData * subDatum;
		RobotShop * subShop;
	};

	struct modelData * datum;
	Fl_Window * window;
	Fl_Output * modelNumber;
	Fl_Input * nameInput;
	Fl_Output * modelWeight;
	Fl_Input * modelMarkup;
	Fl_Output * costInput;
	Fl_Tabs * partsDisplay;
	Fl_Scroll * partListScroller;
		
		
	Fl_Button * subButton;
	//Fl_Button * clrButton;
	Fl_Button * canButton;
	
	static void subModelCB(Fl_Widget * callWidget, struct subData * incSubData);
	static void setNumberCB(Fl_Widget * callWidget, struct modelData * locDatum);
	static void setNameCB(Fl_Widget * callWidget, struct modelData * locDatum);
	static void setMarkupCB(Fl_Widget * callWidget, struct modelData * locDatum);

	newModelWindow(RobotShop * incShop);
	newModelWindow() : newModelWindow(NULL) {};
};


class shopHandler: public RobotShop, public UserInterface {
public:
	RobotShop * currentShop;
	UserInterface * currInterface;
	UserWindow * userWindow;

	shopHandler();
};
#endif
