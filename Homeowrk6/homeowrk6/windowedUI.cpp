#include "stdafx.h"
#include "RobotShop.h"
#include "UserInterfacing.h"
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Widget.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Multiline_Output.H>
#include <FL/Fl_Multiline_Input.H>
#include <FL/Fl_Menu_Window.H>
#include <FL/Fl_Text_Display.H>
#include <FL/Fl_Tabs.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Menu_Item.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Scroll.H>;
#include <vector>
#include "windowedUI.h"

using namespace std;


partChoiceWindow::partChoiceWindow(RobotShop * incShop){
	currentShop = incShop;
	window = new Fl_Window(400, 300, "New Part Type Chooser");
	window->begin();
	instruct = new Fl_Box(100, 50, 200, 30, "Please select the type of part you wish to create.");
	Fl_Choice * partType = new Fl_Choice(100, 100, 100, 30);
	partType->add(" ");
	partType->add("Head");
	partType->add("Arm");
	partType->add("Torso");
	partType->add("Battery");
	partType->add("Locomotor");

	Fl_Button * canButton = new Fl_Button(300,220,80,30,"Cancel");
	canButton->callback((Fl_Callback*)&CloseCB,incShop);
	partType->callback((Fl_Callback*)&typeChosenCB,currentShop);
	window->end();
	window->show();
}

void partChoiceWindow::typeChosenCB(Fl_Widget*callWidget, RobotShop * incShop) {
	Fl_Choice * incWidg = (Fl_Choice*)callWidget;
	
	switch (incWidg->value()) {
	case(1): {newHeadWindow hpartWindow(incShop); break; }
	case(2): {newArmWindow apartWindow(incShop); break; }
	case(3): {newTorsoWindow tpartWindow(incShop); break;}
	case(4): {newBatteryWindow bpartWindow(incShop); break; }
	case(5): {newLocomWindow lpartWindow(incShop); break; }
	}
}

void newModelWindow::subModelCB(Fl_Widget * callWidget, struct subData * incSubData) {
	vector<string> passingData;

	passingData.push_back(incSubData->subDatum->name);

	passingData.push_back(incSubData->subDatum->number);
	passingData.push_back(incSubData->subDatum->markup);
	for(int partIterator=0;partIterator<incSubData->subDatum->robotPartNums.size();partIterator++)
	passingData.push_back(incSubData->subDatum->robotPartNums[partIterator]);
	
	incSubData->subShop->createRobotModel(passingData);
	
	incSubData->subShop->saveState();
	
	while (!(callWidget->as_window())) {
		callWidget = callWidget->parent();
	}
	Fl_Window *callingWindow = (Fl_Window*)callWidget;
	callingWindow->hide();
}

void newModelWindow::setNameCB(Fl_Widget * callWidget, struct modelData * locDatum) {
	Fl_Input* inputWidget = (Fl_Input*)callWidget;
	string tempStr(inputWidget->value());
	locDatum->name = tempStr;
}

void newModelWindow::setNumberCB(Fl_Widget * callWidget, struct modelData * locDatum) {
	Fl_Output* inputWidget = (Fl_Output*)callWidget;
	string tempStr(inputWidget->value());
	locDatum->number = tempStr;
}
void newModelWindow::setMarkupCB(Fl_Widget * callWidget, struct modelData * locDatum) {
	Fl_Input* inputWidget = (Fl_Input*)callWidget;
	string tempStr(inputWidget->value());
	locDatum->markup = tempStr;
}


newModelWindow::newModelWindow(RobotShop * incShop) {
	datum = new struct modelData();
	
	window = new Fl_Window(600, 700, "Create Robot Model");
	window->begin();
	
	modelNumber = new Fl_Output(150, 50, 100, 30, "Model Number: ");
	modelNumber->value((const char*)to_string(incShop->getMaxModelNumber() + 1).c_str());
	modelNumber->callback((Fl_Callback*)&setNumberCB, datum);
	modelNumber->deactivate();

	nameInput= new Fl_Input(100, 100, 200, 30, "Model Name");
	nameInput->callback((Fl_Callback*)&setNameCB, datum);
	
	modelWeight = new Fl_Output(100, 150, 200, 30, "Part Weight");
	modelWeight->deactivate();

	modelMarkup = new Fl_Input(100, 200, 200, 30, "Model Markup");
	modelMarkup->callback((Fl_Callback*)&setMarkupCB, datum);
	
	costInput = new Fl_Output(100, 250, 200, 30, "Part Cost");
	costInput->deactivate();
	/*
	partsDisplay = new Fl_Tabs(320, 50, 360, 390, "AvailableParts");
	
	for(int partTypeIterator=0;partTypeIterator<5;partTypeIterator++) {
		string tempTabName = "";
		switch (partTypeIterator) {
		case(0): {tempTabName =  "Head"; break; }
		case(1): {tempTabName =  "Arm"; break; }
		case(2): {tempTabName =  "Torso"; break; }
		case(3): {tempTabName =  "Battery"; break; }
		case(4): {tempTabName =  "Locomotor"; break; }
		default: {tempTabName = "Invalid Part"; break;}
		}
	   Fl_Group*  currParts = new Fl_Group(321, 51, 359, 389, tempTabName.c_str());
	   {
		   partListScroller = new Fl_Scroll(322, 52, 358, 388);
		   int ypos = 52;
		   partListScroller->begin();
		   ypos += 1;
		   for (int partCounter = 0; partCounter < incShop->partInventory[2].size(); partCounter++) {
			   ypos += 30;
			   int pnum = incShop->partInventory[partTypeIterator][partCounter]->getPartNumber();
			   string pnam = incShop->partInventory[partTypeIterator][partCounter]->getPartName();
			   string tempstr = to_string(pnum);
			   const char* tempChars = tempstr.c_str();
			   Fl_Button * cola = new Fl_Button(323, ypos, 40, 30, "Add");
			   Fl_Output * coln = new Fl_Output(363, ypos, 100, 30, ""); coln->copy_label(pnam.c_str()); coln->deactivate();
			   Fl_Button * colp = new Fl_Button(323, ypos, 80, 30, ""); colp->copy_label(tempChars);
			   //colp->callback((Fl_Callback*)&partInfoCB, incModel->robotParts[partCounter]);
		   }
		   partListScroller->end();
	   }
	}
	*/
	subButton = new Fl_Button(20, 450, 50, 30, "Submit");
	struct subData * lSubData = new subData; lSubData->subDatum = datum; lSubData->subShop = incShop;
	subButton->callback((Fl_Callback*)&subModelCB, lSubData);

	//clrButton = new Fl_Button(450, 450, 50, 30, "Clear");
	canButton = new Fl_Button(520, 450, 60, 30, "Cancel");
	
	canButton->callback((Fl_Callback*)&CloseCB,incShop);
	window->end();
	window->show();
	
}

newHeadWindow::newHeadWindow(RobotShop * incShop) {
	datum->type = "0";
	datum->spec = " ";
	partNumber->value((const char*)to_string(incShop->getMaxPartNumber(0)+1).c_str());
	partNumber->redraw();
	window->label("Create New Head Part");
	struct subData * lSubData = new subData; lSubData->subDatum = datum; lSubData->subShop = incShop;
	subButton->callback((Fl_Callback*)&subPartCB, lSubData);
	window->redraw();
	window->show();
}

newArmWindow::newArmWindow(RobotShop * incShop) {
	datum->type = "1";
	partNumber->value((const char*)to_string(incShop->getMaxPartNumber(1) + 1).c_str());
	partNumber->redraw();
	window->label("Create New Arm Part");
	armInput = new Fl_Input(140, 270, 200, 30, "Power Consumption");
	armInput->callback((Fl_Callback*)&setSpecCB, datum);
	struct subData * lSubData = new subData; lSubData->subDatum = datum; lSubData->subShop = incShop;
	subButton->callback((Fl_Callback*)&subPartCB, lSubData);
	window->add(armInput);
	window->redraw();
	window->show();
}

newTorsoWindow::newTorsoWindow(RobotShop * incShop) {
	datum->type = "2";
	partNumber->value((const char*)to_string(incShop->getMaxPartNumber(2) + 1).c_str());
	partNumber->redraw();
	window->label("Create New Torso Part");
	torsoInput = new Fl_Input(140, 270, 200, 30, "Max No. of Batteries");
	torsoInput->callback((Fl_Callback*)&setSpecCB, datum);
	window->add(torsoInput);
	struct subData * lSubData = new subData; lSubData->subDatum = datum; lSubData->subShop = incShop;
	subButton->callback((Fl_Callback*)&subPartCB, lSubData);
	window->add(torsoInput);
	window->redraw();
	window->show();
}

newBatteryWindow::newBatteryWindow(RobotShop * incShop) {
	datum->type = "3";
	partNumber->value((const char*)to_string(incShop->getMaxPartNumber(3) + 1).c_str());
	partNumber->redraw();
	window->label("Create New Battery Part");
	batteryInput = new Fl_Input(140, 270, 200, 30, "Total Energy");
	batteryInput->callback((Fl_Callback*)&setSpecCB, datum);
	window->add(batteryInput);
	struct subData * lSubData = new subData; lSubData->subDatum = datum; lSubData->subShop = incShop;
	subButton->callback((Fl_Callback*)&subPartCB, lSubData);
	window->redraw();
	window->show();
}

newLocomWindow::newLocomWindow(RobotShop * incShop) {
	datum->type = "4";
	partNumber->value((const char*)to_string(incShop->getMaxPartNumber(4) + 1).c_str());
	partNumber->redraw();
	locomInput = new Fl_Input(140, 270, 200, 30, "Max Movement Speed");
	locomInput->callback((Fl_Callback*)&setSpecCB, datum);
	window->label("Create New Locomotor Part");
	window->add(locomInput);
	struct subData * lSubData = new subData; lSubData->subDatum = datum; lSubData->subShop = incShop;
	subButton->callback((Fl_Callback*)&subPartCB, lSubData);
	window->redraw();
	window->show();
}

void newPartWindow::subPartCB(Fl_Widget * callWidget, struct subData * incSubData){
	vector<string> passingData;

	passingData.push_back(incSubData->subDatum->name);
	
	passingData.push_back(incSubData->subDatum->type);
	passingData.push_back("0");
	passingData.push_back(incSubData->subDatum->desc);
	passingData.push_back(incSubData->subDatum->weight);
	passingData.push_back(incSubData->subDatum->cost);
	passingData.push_back(incSubData->subDatum->spec);

	incSubData->subShop->createRobotPart(passingData, atoi(incSubData->subDatum->type.c_str()));
	incSubData->subShop->saveState();
	while (!(callWidget->as_window())) {
		callWidget = callWidget->parent();
	}
	Fl_Window *callingWindow = (Fl_Window*)callWidget;
	callingWindow->hide();
}

void newPartWindow::setNameCB(Fl_Widget * callWidget, struct partData * locDatum) {
	Fl_Input* inputWidget = (Fl_Input*)callWidget;
	string tempStr(inputWidget->value());
	locDatum->name= tempStr;
}

void newPartWindow::setNumberCB(Fl_Widget * callWidget, struct partData * locDatum) {
	Fl_Output* inputWidget = (Fl_Output*)callWidget;
	string tempStr(inputWidget->value());
	locDatum->number = tempStr;
}
void newPartWindow::setWeightCB(Fl_Widget * callWidget, struct partData * locDatum) {
	Fl_Input* inputWidget = (Fl_Input*)callWidget;
	string tempStr(inputWidget->value());
	locDatum->weight = tempStr;
}
void newPartWindow::setCostCB(Fl_Widget * callWidget, struct partData * locDatum) {
	Fl_Input* inputWidget = (Fl_Input*)callWidget;
	string tempStr(inputWidget->value());
	locDatum->cost = tempStr;
}
void newPartWindow::setDescCB(Fl_Widget * callWidget, struct partData * locDatum) {
	Fl_Input* inputWidget = (Fl_Input*)callWidget;
	string tempStr(inputWidget->value());
	locDatum->desc = tempStr;
}
void newPartWindow::setSpecCB(Fl_Widget * callWidget, struct partData * locDatum) {
	Fl_Input* inputWidget = (Fl_Input*)callWidget;
	string tempStr(inputWidget->value());
	locDatum->spec = tempStr;
}





newPartWindow::newPartWindow(RobotShop * incShop) {
	currentShop = incShop;
	datum = new struct partData();

	window = new Fl_Window(600, 500, "Create Robot Part");
	window->begin();

	partNumber = new Fl_Output(140, 70, 200, 30, "Part Number: ");
	partNumber->callback((Fl_Callback*)&setNumberCB, datum);
	nameInput = new Fl_Input(140, 120, 200, 30, "Part Name");
	nameInput->callback((Fl_Callback*)&setNameCB, datum);
	weightInput = new Fl_Input(140, 170, 200, 30, "Part Weight");
	weightInput->callback((Fl_Callback*)&setWeightCB, datum);
	costInput = new Fl_Input(140, 220, 200, 30, "Part Cost");
	costInput->callback((Fl_Callback*)&setCostCB, datum);

	descInput = new Fl_Input(140, 320, 200, 30, "Part Description");
	descInput->callback((Fl_Callback*)&setDescCB, datum);
	subButton = new Fl_Button(20, 450, 50, 30, "Submit");
	//clrButton = new Fl_Button(450, 450, 50, 30, "Clear");
	canButton = new Fl_Button(520, 450, 60, 30, "Cancel");

	canButton->callback((Fl_Callback*)&CloseCB, incShop);
	window->end();

}


moreInfoWindow::moreInfoWindow(RobotModel* incModel) {
	Fl_Window*  window = new Fl_Window(600, 500, "Model Information");
	window->begin();
	Fl_Output * modelName = new Fl_Output(250, 10, 100, 40, "Model Name"); modelName->value(incModel->getModelName().c_str());
	Fl_Output * modelNum = new Fl_Output(150, 70, 100, 30, "Model Number"); modelNum->value(to_string(incModel->getModelNumber()).c_str());
	Fl_Output * modelWt = new Fl_Output(350, 70, 100, 30, "Model Weight"); modelWt->value(to_string(incModel->getRobotWeight()).c_str());
	Fl_Output * modelPrice = new Fl_Output(150, 120, 100, 30, "Model Price"); modelPrice->value(to_string(incModel->getRobotPrice()	).c_str());
	
	//Fl_Output * modelMrkUp = new Fl_Output(110, 120, 100, 30, "Model Markup"); modelNum->value(to_string(incModel->getRobotMarkup()).c_str());
	int ypos = 170;
	Fl_Box * partsBox = new Fl_Box(160, ypos, 100, 30, "Parts List");
	Fl_Scroll * scroller = new Fl_Scroll(179, ypos + 30, 91, 450 - (ypos + 40));
	scroller->begin();
	  ypos += 2;
	  for (int partCounter = 0; partCounter < incModel->robotParts.size(); partCounter++) {
	   ypos += 30;
	   int pn = incModel->robotParts[partCounter]->getPartNumber();
	   string tempstr = to_string(pn);
	   const char* tempChars = tempstr.c_str();
	   Fl_Button * colp = new Fl_Button(180, ypos, 80, 30, ""); colp->copy_label(tempChars);
	   colp->callback((Fl_Callback*)&partInfoCB, incModel->robotParts[partCounter]);	
	  }
	scroller->end();
	
	Fl_Button * okButton = new Fl_Button(20, 450, 50, 30, "Okay");
	okButton->callback((Fl_Callback*)&CloseCB, NULL);

		window->end();
	window->show();

}

moreInfoWindow::moreInfoWindow(RobotPart* incPart) {
	Fl_Window*  window = new Fl_Window(600, 500, "Part Information");
	window->begin();
	Fl_Output * partName = new Fl_Output(250, 10, 100, 40, "Part Name"); partName->value(incPart->getPartName().c_str());
	
	Fl_Output * partNum = new Fl_Output(150, 70, 100, 30, "Part Number"); partNum->value(to_string(incPart->getPartNumber()).c_str());
	Fl_Output * partType = new Fl_Output(350, 70, 100, 30, "Part Type"); partType->value(incPart->getPartTypeStr().c_str());

	Fl_Output * partWt = new Fl_Output(150, 110, 100, 30, "Part Weight"); partWt->value(to_string(incPart->getPartWeight()).c_str()); 
	Fl_Output * partCost = new Fl_Output(350, 110, 100, 30, "Part Cost"); partCost->value(to_string(incPart->getPartCost()).c_str());
	
	
	Fl_Output * partSpec = new Fl_Output(150, 150, 100, 30, "");
	if (incPart->getPartTypeInt() == 0) partSpec->clear_visible();

	switch (incPart->getPartTypeInt()) {
	 case(0): {Head* tempPart = (Head*)incPart; break; }
	 case(1): {Arm* tempPart = (Arm*)incPart;  partSpec->copy_label("Energy Consumption");  partSpec->value(to_string(tempPart->getPowerConsumption()).c_str()); break; }
	 case(2): {Torso* tempPart = (Torso*)incPart;  partSpec->copy_label("Battery Compartments"); partSpec->value(to_string(tempPart->getMaxBatterCompartments()).c_str()); break; }
	 case(3): {Battery* tempPart = (Battery*)incPart;  partSpec->copy_label("Part Power"); partSpec->value(to_string(tempPart->getEnergy()).c_str()); break; }
	 case(4): {Locomotor* tempPart = (Locomotor*)incPart;  partSpec->copy_label("Max Speed"); partSpec->value(to_string(tempPart->getMaxSpeed()).c_str()); break; }
	}

	Fl_Multiline_Output * partDesc = new Fl_Multiline_Output(110, 190, 300, 100, "Part Description"); partDesc->value(incPart->getPartDescription().c_str()); partDesc->wrap(1);
	
	Fl_Button * okButton = new Fl_Button(20, 450, 50, 30, "Okay");
	okButton->callback((Fl_Callback*)&CloseCB, NULL);

	window->end();
	window->show();
}


void reportWindow::modelInfoCB(Fl_Widget * callWidget, RobotModel* incModel) {
	moreInfoWindow moreInfo(incModel);
}

void reportWindow::partInfoCB(Fl_Widget * callWidget, RobotPart* incPart) {
	moreInfoWindow moreInfo(incPart);
}

reportModelsWindow::reportModelsWindow(RobotShop * incShop) {
	window = new Fl_Window(940, 400, "Robot Parts Catalog");
	int ypos = 20;
	window->begin();

	Fl_Box * col1 = new Fl_Box(20, ypos, 120, 30, "Model Name");
	Fl_Box * col2 = new Fl_Box(140, ypos, 100, 30, "Model Number");
	Fl_Box * col3 = new Fl_Box(240, ypos, 100, 30, "Model Price");
	Fl_Box * col4 = new Fl_Box(340, ypos, 500, 30, "Parts");
	Fl_Box * col5 = new Fl_Box(840, ypos, 80, 30, "");
	Fl_Scroll * scroller = new Fl_Scroll(0, ypos + 30, 930, 350 - (ypos + 40));
	scroller->begin();
	ypos += 2;
		for (int robotCounter = 0; robotCounter < incShop->robotInventory.size(); robotCounter++) {
			ypos += 30;
			Fl_Output * col1 = new Fl_Output(20, ypos, 120, 30); col1->value((incShop->robotInventory[robotCounter]->getModelName()).c_str());
			Fl_Output * col2 = new Fl_Output(140, ypos, 100, 30); col2->value(to_string(incShop->robotInventory[robotCounter]->getModelNumber()).c_str());
			Fl_Output * col3 = new Fl_Output(240, ypos, 100, 30); col3->value(to_string(incShop->robotInventory[robotCounter]->getRobotPrice()).c_str());
			int xpos = 340;
			for (int partCounter = 0; partCounter < incShop->robotInventory[robotCounter]->robotParts.size(); partCounter++) {
				int pn = incShop->robotInventory[robotCounter]->robotParts[partCounter]->getPartNumber();
				string tempstr = to_string(pn);
				const char* tempChars = tempstr.c_str();
				Fl_Button * colp = new Fl_Button(xpos, ypos, 80, 30, ""); colp->copy_label(tempChars);
				colp->callback((Fl_Callback*)&partInfoCB, incShop->robotInventory[robotCounter]->robotParts[partCounter]);
				xpos += 80;
			}
			Fl_Button * colb = new Fl_Button(840, ypos, 80, 30, "More Info");
			colb->callback((Fl_Callback*)&modelInfoCB, incShop->robotInventory[robotCounter]);
		}
		scroller->end();
	okButton = new Fl_Button(20, 350, 50, 30, "Okay");
	okButton->callback((Fl_Callback*)&CloseCB, incShop);
	window->end();
	window->show();
}

reportPartsWindow::reportPartsWindow(RobotShop * incShop) {
	window = new Fl_Window(800, 400, "Robot Parts Catalog");
	int ypos = 20;
	int rows = (const int)incShop->partInventory->size();
	window->begin();
	
	Fl_Box * col1 = new Fl_Box(20, ypos, 100, 30, "Part Name"); 
	Fl_Box * col2 = new Fl_Box(120, ypos, 80, 30, "Part Type");
	Fl_Box * col3 = new Fl_Box(200, ypos, 80, 30, "Part Number");
	Fl_Box * col4 = new Fl_Box(280, ypos, 400, 30, "Description");
	Fl_Box * col5 = new Fl_Box(680, ypos, 80, 30, "");
	Fl_Scroll * scroller = new Fl_Scroll(0, ypos + 30, 780, 350 - (ypos + 40));
	scroller->begin();
	ypos += 2;
	for(int partTypeCounter = 0; partTypeCounter<5;partTypeCounter++)
	 for (int partsCounter = 0; partsCounter < incShop->partInventory[partTypeCounter].size(); partsCounter++) {
		 ypos += 30;
		 Fl_Output * col1 = new Fl_Output(20, ypos, 100, 30); col1->value((incShop->partInventory[partTypeCounter][partsCounter]->getPartName()).c_str());
		Fl_Output * col2 = new Fl_Output(120, ypos, 80, 30); col2->value((incShop->partInventory[partTypeCounter][partsCounter]->getPartTypeStr()).c_str());
		Fl_Output * col3 = new Fl_Output(200, ypos, 80, 30); col3->value(to_string(incShop->partInventory[partTypeCounter][partsCounter]->getPartNumber()).c_str());
		Fl_Output * col4 = new Fl_Output(280, ypos, 400, 30); col4->value((incShop->partInventory[partTypeCounter][partsCounter]->getPartDescription()).c_str());
		Fl_Button * col5 = new Fl_Button(680, ypos, 80, 30, "More Info");
		col5->callback((Fl_Callback*)&partInfoCB, incShop->partInventory[partTypeCounter][partsCounter]);
	}
	scroller->end();
	okButton = new Fl_Button(20,350,50,30,"Okay");
	okButton->callback((Fl_Callback*)&CloseCB, incShop);
	window->end();
	window->show();
}


custMenuWindow::custMenuWindow(RobotShop * incShop) {
	window = new Fl_Window(400, 600, "Customer Menu");
	window->begin();
	menuBar = new Fl_Menu_Bar(0, 0, 600, 30);
	
	menuBar->add("&Home", 0, 0, 0, FL_SUBMENU);
	menuBar->add("Home/Main Men&u", FL_ALT + 'u', (Fl_Callback*)&mainCB, incShop);
	menuBar->add("Home/&Exit", 0, (Fl_Callback*)&CloseCB, incShop);
	menuBar->add("&Robots", 0, 0, 0, FL_SUBMENU);
	menuBar->add("Robots/&View Robot Models", 0, (Fl_Callback*)&viewModelCB, incShop);
	menuBar->add("&Orders", FL_ALT + 'o', 0, 0, FL_SUBMENU);
	menuBar->add("Orders/&New Order",FL_ALT + 'n',0,0,FL_MENU_INACTIVE);
	menuBar->add("Orders/M&y Orders", FL_ALT + 'y', 0, 0, FL_MENU_INACTIVE);
	window->end();
	window->show();
}

saMenuWindow::saMenuWindow(RobotShop * incShop) {
	currentShop = incShop;
	window = new Fl_Window(400, 600, "Sales Associate Menu");
	window->begin();
	menuBar = new Fl_Menu_Bar(0, 0, 600, 30);
	menuBar->add("&Home", 0, 0, 0, FL_SUBMENU);
	menuBar->add("Home/Main Men&u", FL_ALT + 'u', (Fl_Callback*)&mainCB, incShop);
	menuBar->add("Home/&Exit", 0, (Fl_Callback*)&CloseCB, incShop);
	menuBar->add("&Robots", 0, 0, 0, FL_SUBMENU);
	menuBar->add("Robots/&View Robot Models", 0, (Fl_Callback*)&viewModelCB, incShop);
	menuBar->add("&Orders", FL_ALT + 'o', 0, 0, FL_SUBMENU);
	menuBar->add("Orders/&New Order", FL_ALT + 'n', 0, 0, FL_MENU_INACTIVE);
	menuBar->add("Orders/M&y Orders", FL_ALT + 'y', 0, 0, FL_MENU_INACTIVE);
	window->end();
	window->show();
}

pmMenuWindow::pmMenuWindow(RobotShop * incShop) {
	window = new Fl_Window(400, 600, "Product Manager Menu");
	window->begin();
	menuBar = new Fl_Menu_Bar(0, 0, 600, 30);
	menuBar->add("&Home", 0, 0, 0, FL_SUBMENU);
	menuBar->add("Home/Main Men&u", FL_ALT + 'u', (Fl_Callback*)&mainCB, incShop);
	menuBar->add("Home/&Exit", 0, (Fl_Callback*)&CloseCB, incShop);
	menuBar->add("&Robots", 0, 0, 0, FL_SUBMENU);
	menuBar->add("Robots/View Robot &Parts", 0,(Fl_Callback*)&viewPartCB, incShop);
	menuBar->add("Robots/Ne&w Robot Part", 0, (Fl_Callback*)&createPartCB, incShop);//createCB
	menuBar->add("Robots/&View Robot Models", 0, (Fl_Callback*)&viewModelCB, incShop);
	menuBar->add("Robots/N&ew Robot Model", 0, (Fl_Callback*)&createModelCB, incShop);
	menuBar->add("&Orders", FL_ALT + 'o', 0, 0, FL_SUBMENU);
	menuBar->add("Orders/&New Order", FL_ALT + 'n', 0, 0, FL_MENU_INACTIVE);
	menuBar->add("Orders/M&y Orders", FL_ALT + 'y', 0, 0, FL_MENU_INACTIVE);
	window->end();
	window->show();
}

phbMenuWindow::phbMenuWindow(RobotShop * incShop) {
	window = new Fl_Window(400, 600, "Pointy Haired Boss Menu");
	window->begin();
	menuBar = new Fl_Menu_Bar(0, 0, 600, 30);
	menuBar->add("&Home", 0, 0, 0, FL_SUBMENU);
	menuBar->add("Home/Main Men&u", FL_ALT + 'u', (Fl_Callback*)&mainCB, incShop);
	menuBar->add("Home/&Exit", 0, (Fl_Callback*)&CloseCB, incShop);
	menuBar->add("&Robots", 0, 0, 0, FL_SUBMENU);
	menuBar->add("Robots/&View Robot Models", 0, (Fl_Callback*)&viewModelCB, incShop);
	menuBar->add("&Orders", FL_ALT + 'o', 0, 0, FL_SUBMENU);
	menuBar->add("Orders/&New Order", FL_ALT + 'n', 0, 0, FL_MENU_INACTIVE);
	menuBar->add("Orders/M&y Orders", FL_ALT + 'y', 0, 0, FL_MENU_INACTIVE);
	window->end();
	window->show();
}


void EmplWindow::saEntryCB(Fl_Widget * callWidget, RobotShop * incShop) {
	while (!(callWidget->as_window())) {
		callWidget = callWidget->parent();
	}
	Fl_Window *callingWindow = (Fl_Window*)callWidget;
	callingWindow->hide();
	saMenuWindow saWindow(incShop);
}

void EmplWindow::pmEntryCB(Fl_Widget * callWidget, RobotShop * incShop) {
	while (!(callWidget->as_window())) {
		callWidget = callWidget->parent();
	}
	Fl_Window *callingWindow = (Fl_Window*)callWidget;
	callingWindow->hide();
	pmMenuWindow saWindow(incShop);
}

void EmplWindow::phbEntryCB(Fl_Widget * callWidget, RobotShop * incShop) {
	while (!(callWidget->as_window())) {
		callWidget = callWidget->parent();
	}
	Fl_Window *callingWindow = (Fl_Window*)callWidget;
	callingWindow->hide();
	phbMenuWindow phbWindow(incShop);
}


EmplWindow::EmplWindow(RobotShop * incShop) {
	currentShop = incShop;
	window = new Fl_Window(400, 400, "Employee Type");
	window->begin();
	menuGroup = new Fl_Group(50, 50, 300, 200, "What is your employee role?");
	menuGroup->begin();
	saButton = new Fl_Button(25, 60, 200, 50, "I am a Sales Associate.");
	saButton->callback((Fl_Callback*)&saEntryCB, incShop);
	pmButton = new Fl_Button(25, 120, 200, 50, "I am an Product Manager.");
	pmButton->callback((Fl_Callback*)&pmEntryCB, incShop);
	phbButton = new Fl_Button(25, 180, 200, 50, "I am THE Pointy-Haired Boss!");
	phbButton->callback((Fl_Callback*)&phbEntryCB, incShop);
	menuGroup->end();
	window->end();
	window->show();
}

void userMenuWindow::createPartCB(Fl_Widget * callWidget, RobotShop * incShop) {
	partChoiceWindow choose(incShop);
}

void userMenuWindow::createModelCB(Fl_Widget * callWidget, RobotShop * incShop) {
	newModelWindow newModel(incShop);
}

void userMenuWindow::viewPartCB(Fl_Widget * callWidget, RobotShop * incShop) {
	reportPartsWindow report(incShop);
}

void userMenuWindow::viewModelCB(Fl_Widget * callWidget, RobotShop * incShop) {
	reportModelsWindow report(incShop);
}

void UserWindow::custEntryCB(Fl_Widget * callWidget, RobotShop * incShop) {
	while (!(callWidget->as_window())) {
		callWidget = callWidget->parent();
	}
	Fl_Window *callingWindow = (Fl_Window*)callWidget;
	callingWindow->hide();
	custMenuWindow custWindow(incShop);
}
void UserWindow::emplEntryCB(Fl_Widget * callWidget, RobotShop * incShop) {
	while (!(callWidget->as_window())) {
		callWidget = callWidget->parent();
	}
	Fl_Window *callingWindow = (Fl_Window*)callWidget;
	callingWindow->hide();
	EmplWindow emplWindow(incShop);
}

 UserWindow::UserWindow(RobotShop * incShop) {
	window =new Fl_Window(400, 400, "User Welcome");
	window->begin();
	menuGroup = new Fl_Group(50, 50, 200, 200, "Tell us who you are.");
	menuGroup->begin();
	custButton = new Fl_Button(25, 90, 150, 50, "I am a Customer.");
	custButton->callback((Fl_Callback*)&custEntryCB,incShop);
	emplButton = new Fl_Button(25, 150, 150, 50, "I am an employee");
	emplButton->callback((Fl_Callback*)&emplEntryCB,incShop);
	menuGroup->end();
	window->end();
	window->show();
}

 void menuWindow::mainCB(Fl_Widget * callWidget, RobotShop * incShop) {
	 while (!(callWidget->as_window())) {
		 callWidget = callWidget->parent();
	 }
	 Fl_Window * callingWindow = (Fl_Window*)callWidget;
	 callingWindow->hide();
	 UserWindow firstWindow(incShop);
 }
 
void menuWindow::CloseCB(Fl_Widget* callWidget, RobotShop * incShop) {
	while (!(callWidget->as_window())) {
		callWidget = callWidget->parent();
	}
	Fl_Window * callingWindow = (Fl_Window*)callWidget;
	incShop->saveState();
	callingWindow->hide();

	exit;
}


shopHandler::shopHandler() {
	currentShop = new RobotShop();
	currInterface = new UserInterface();
	userWindow = new UserWindow(currentShop);

}


int main() {
	shopHandler controller = shopHandler();
	
	return Fl::run();
	
}
