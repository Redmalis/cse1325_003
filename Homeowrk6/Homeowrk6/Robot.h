#ifndef __Robot_H
#define __Robot_H 2016

class PartType {
public:
	PartType();
	PartType(int incTypeVal);
	static const int head = 0;
	static const int arm = 1;
	static const int torso = 2;
	static const int battery = 3;
	static const int locomotor = 4;
	string to_string();
	int to_int();

private:
	int typeVal;
	int numTypes;
};

class RobotPart {
public:
	RobotPart();
	string getPartName();
	void setPartName(string incName);
	int getPartNumber();
	void setPartNumber(int incNumber);
	double getPartWeight();
	void setPartWeight(double incWeight);
	double getPartCost();
	void setPartCost(double incCost);
	string getPartDescription();
	void setPartDescription(string incDescription);
	//Image getPartImage();
	//void setPartImage(Image);
	string getPartTypeStr();
	int getPartTypeInt();
	void setPartType(int incType);
	virtual string getPartString(int shortOrLong, char screenOrFile);
	

protected:
	string getPartStringLong(char screenOrFile);
	string getPartStringShort(char screenOrFile);
	string basePartString(char screenOrFile);
	string partName;
	int partNumber;
	PartType partType;
	double partWeight;
	double partCost;
	string partDescription;
	//Image partImage;
};

class RobotModel {
public:
	RobotModel(vector<string> incModelInformation);
	string getModelName();
	void setModelName(string incName);
	int getModelNumber();
	void setModelNumber(int incNumber);
	double getRobotWeight();
	void setRobotMarkup(double incMarkup);
	double getRobotPrice();
	double getRobotMarkup();
	string getModelString(char screenOrFile);
	void addRobotPart(RobotPart* newPart);
	double calculatePartsTotalCost();
	void setHasTorso();
	int getHasTorso();
	vector<RobotPart *> robotParts;

private:
	
	int partTypeMaxes[5] = { 1, 2, 1, 3, 1 };
	int hasTorso = 0;
	string modelName;
	int modelNumber;
	double robotMarkUp;
	
	//ProductManager creatorPM;
};

class Head : public RobotPart {
public:
	Head(vector<string> incPartInformation);
private:
};

class Arm : public RobotPart {
public:
	Arm(vector<string> incPartInformation);
	void setPowerConsumption(int incConsumption);
	int getPowerConsumption();
	string getPartStringLong(char screenOrFile);
	string getPartString(int shortOrLong, char screenOrFile);

private:
	int powerConsumption;
};

class Battery : public RobotPart {
public:
	Battery(vector<string> incPartInformation);
	void setEnergy(double incEnergy);
	double getEnergy();
	string getPartStringLong(char screenOrFile);
	string getPartString(int shortOrLong, char screenOrFile);

private:
	double energy;
};

class Torso : public RobotPart {
public:
	Torso(vector<string> incPartInformation);
	void setMaxBatteryCompartments(int incMax);
	int getMaxBatterCompartments();
	void insertBattery(RobotPart& incBattery);
	string getPartStringLong(char screenOrFile);
	string getPartString(int shortOrLong, char screenOrFile);

private:
	int maxBatteryCompartments;
	vector<RobotPart*> batteries;
};

class Locomotor : public RobotPart {
public:
	Locomotor(vector<string> incPartInformation);
	void setMaxSpeed(int incMaxSpeed);
	int getMaxSpeed();
	string getPartStringLong(char screenOrFile);
	string getPartString(int shortOrLong, char screenOrFile);
private:
	int maxSpeed;
};

#endif
