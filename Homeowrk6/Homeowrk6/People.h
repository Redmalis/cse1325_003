#ifndef __People_H
#define __People_H 2016


class Person {
public:
	string getName();
	void setName(string incName);

protected:
	string name;
};

class Customer : Person {
public:
	int getCustomerNumber();
	void setCustomerNumber(int incNumber);
	double getWallet();
	void setWallet(double incWallet);
	
private:
	int customerNumber;
	double wallet;
	
};

class SalesAssociate : Person {
public:
	int getEmployeeNumber();
	void setEmployeeNumber(int incNumber);
	//void addOrder(Order incOrder);
	//vector<Order> retrieveOrders();
private:
	int employeeNumber;
	double wallet;
	//vector <Order> orders;
};

class ProductManager : Person {
public:
	int getEmployeeNumber();
	void setEmployeeNumber(int incNumber);
	string ProductManager::getString(char screenOrFile);
	//void addRobotModel(RobotModel incRobot);
	//vector<RobotModel> retrieveRobotModels();

private:
	int employeeNumber;
	//vector<RobotModel> robots;
};
#endif