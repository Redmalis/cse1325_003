#ifndef __RobotShop_H
#define __RobotShop_H 2016
#include "Robot.h"


class RobotShop {
public:
	RobotShop();
	void createRobotPart(vector<string> incPartInformation, int incType);
	void createRobotModel();
	void createRobotModel(vector<string> incModelVector);
	void inventoryNewPart(RobotPart* incNewPart);
	void saveState();
	int getMaxPartNumber(int incTypeVal);
	int getMaxModelNumber();
	//Order* startOder();
	//Customer* createCustomer();

	vector<RobotPart *> partInventory[5];
	vector<RobotModel *> robotInventory;
	vector<vector<string>> runReport(int reportNum);

private:
	
	RobotPart * getInventoryPart(int incPartNum);
	RobotModel * getInventoryModel(int incModelNum);
	
	
	
	//vector<vector<string>> listModels();

	vector<vector<string>> RobotShop::AssembleData(const vector<RobotPart *> incPartsVector[5],int incTypeVal, int shortOrLong, char screenOrFile);
	vector<vector<string>> RobotShop::AssembleData(const vector<RobotModel *>& incModelVector, char screenOrFile);
	
	void writeData(string incFileName);
	void readData(string incFileName);
	void parsePartData(vector<vector<string>> incData);
	void parseModelData(vector<vector<string>> incData);
	
	
	
	//vector<SalesAssociate*> salesAssociates;
	//vector<ProductManager*> productManager;
	//vector<Order*> orders;
	//vector<Customer*> customers;

};

#endif
