#include "stdafx.h"
#include "UserInterfacing.h"
#include "RobotShop.h"
#include "Robot.h"


RobotShop::RobotShop() {
	readData("PartsData.dat");
	readData("ModelData.dat");
};

void RobotShop::createRobotModel(vector<string> incModelVector) {
	RobotModel * loadingModel = new RobotModel(incModelVector);
	for (int partIterator = 3; partIterator <(int)incModelVector.size(); partIterator++) {
		RobotPart * tempPart = getInventoryPart(atoi(incModelVector[partIterator].c_str()));
		if(tempPart!=NULL) loadingModel->addRobotPart(tempPart);
	}
	robotInventory.push_back(loadingModel);
}

void RobotShop::createRobotModel() {
	int tempPartNum = 0;
	UserInterface modelInterface = UserInterface();
	vector<string> tempModelData;
	tempModelData = modelInterface.menu_2230();
	tempModelData[1] = to_string(getMaxModelNumber() + 1);
	RobotModel * newModel = new RobotModel(tempModelData);
	while (tempPartNum < 6) {
		tempPartNum = modelInterface.menu_2231(tempModelData,newModel->getHasTorso());
		if (tempPartNum > 0 && tempPartNum < 6) {
			try {
				int secondIndexer = modelInterface.menu_2232(AssembleData(partInventory,tempPartNum - 1,0,'s'));
				newModel->addRobotPart(partInventory[tempPartNum - 1][secondIndexer]);
				tempModelData.push_back(partInventory[tempPartNum - 1][secondIndexer]->getPartString(0,'s'));
			}
			catch (exception &e) { 
				char tempChar; 
				cerr <<"\n"<<e.what()<<" Last part select has been discarded.\nPress <enter> to continue."; 
				cin.ignore();
				cin.get(tempChar);
			}
		}
	}
	newModel->setRobotMarkup(modelInterface.menu_2233(newModel->calculatePartsTotalCost()));
	robotInventory.push_back(newModel);
}


/*
vector < vector<string>> RobotShop::listModels() {
	int indexIter = 0;
	vector<vector<string>> modelList;
	while (indexIter < ((int)robotInventory.size())) {
		vector<string> tempVector;
		tempVector.push_back(robotInventory[indexIter]->getModelString());
		modelList.push_back(tempVector);
		indexIter++;
	}
	return modelList;
}

*/


void RobotShop::createRobotPart(vector<string> incPartInformation,int incType){
	if(incPartInformation[2]=="0")
	incPartInformation[2] = to_string(getMaxPartNumber(incType)+1);
	switch (incType) {
	case 0: {inventoryNewPart(new Head(incPartInformation)); break; }
	case 1: {inventoryNewPart(new Arm(incPartInformation)); break; }
	case 2: {inventoryNewPart(new Torso(incPartInformation)); break; }
	case 3: {inventoryNewPart(new Battery(incPartInformation)); break; }
	case 4: {inventoryNewPart(new Locomotor(incPartInformation)); break; }
	default:break;	
	}

}

RobotModel* RobotShop::getInventoryModel(int incModelNum){
	int indexIterator = 0;
	while (indexIterator < (int)robotInventory.size())
		if (robotInventory[indexIterator]->getModelNumber() == incModelNum) return robotInventory[indexIterator];
	return NULL;
}


int RobotShop::getMaxPartNumber(int incTypeVal) {
	
	if (partInventory[incTypeVal].size() > 0)
		return partInventory[incTypeVal][partInventory[incTypeVal].size() - 1]->getPartNumber();
	else return (((incTypeVal+1) * 100000));
}

int RobotShop::getMaxModelNumber() {
	if (robotInventory.size() > 0)
		return robotInventory[robotInventory.size() - 1]->getModelNumber();
	else return (1000000);
}

RobotPart * RobotShop::getInventoryPart(int incPartNum) {
	if (incPartNum > 100000 && incPartNum < 1000000) {
		int indexIterator = 0;
		while (indexIterator < (int)partInventory[(incPartNum/100000)-1].size()) {
			if (partInventory[(incPartNum / 100000)-1][indexIterator]->getPartNumber() == incPartNum) return partInventory[(incPartNum / 100000)-1][indexIterator];
			else indexIterator++;
		}
	}
	return NULL;
}

void RobotShop::inventoryNewPart(RobotPart* incNewPart) {
	partInventory[incNewPart->getPartTypeInt()].push_back(incNewPart);
}

//Order* RobotShop::startOder(){
//};

//Customer* RobotShop::createCustomer(){
//};

void RobotShop::saveState(){
	writeData("PartsData.dat");
	writeData("ModelData.dat");
}

void RobotShop::writeData(string incFileName) {
	UserInterface tempInterface = UserInterface();
	ofstream outputFile;
	try {
		outputFile.open(incFileName, ios::out, ios::trunc);
	}
	catch (exception &e) {}
	if(incFileName.substr(0,5)=="Parts")
		tempInterface.showReport(outputFile, AssembleData(partInventory,10, 1,'f'));
	if (incFileName.substr(0, 5) == "Model")
		tempInterface.showReport(outputFile, AssembleData(robotInventory,'f'));

}

vector<vector<string>> RobotShop::AssembleData(const vector<RobotModel *>& incModelVector, char screenOrFile) {
	vector<vector<string>> dataVector;
	for (int indexIter = 0; indexIter < ((int)incModelVector.size()); indexIter++) {
		vector<string> tempDataVector;
		tempDataVector.push_back(incModelVector[indexIter]->getModelString(screenOrFile));
		dataVector.push_back(tempDataVector);
	}
	return dataVector;
}

vector<vector<string>> RobotShop::AssembleData(const vector<RobotPart *> incPartsVector[5], int incTypeVal, int shortOrLong, char screenOrFile) {
	vector < vector<string>> dataVector;
	for (int indexIter = 0; indexIter < 5; indexIter++) {
		if ((incTypeVal < 5 && indexIter == incTypeVal) || incTypeVal > 9) {
			vector<string> insideDataVector;
			for (int partIterator = 0; partIterator < (int)partInventory[indexIter].size(); partIterator++) {
				switch (indexIter) {
					case(0): {Head* tempPart = (Head*)partInventory[indexIter][partIterator]; insideDataVector.push_back(tempPart->getPartString(shortOrLong, screenOrFile)); break; }
					case(1): {Arm* tempPart = (Arm*)partInventory[indexIter][partIterator]; insideDataVector.push_back(tempPart->getPartString(shortOrLong, screenOrFile)); break; }
					case(2): {Torso* tempPart = (Torso*)partInventory[indexIter][partIterator]; insideDataVector.push_back(tempPart->getPartString(shortOrLong, screenOrFile)); break; }
					case(3): {Battery* tempPart = (Battery*)partInventory[indexIter][partIterator]; insideDataVector.push_back(tempPart->getPartString(shortOrLong, screenOrFile)); break; }
					case(4): {Locomotor* tempPart = (Locomotor*)partInventory[indexIter][partIterator]; insideDataVector.push_back(tempPart->getPartString(shortOrLong, screenOrFile)); break; }
				}
				if ((int)insideDataVector.size() > 0) { dataVector.push_back(insideDataVector); }
			}
		}
	}
	return dataVector;
}

void RobotShop::parsePartData(vector<vector<string>> incData){
	for (int indexIter = 0; indexIter < ((int)incData.size()); indexIter++) {
	//	if (incData[indexIter].size() > 6) {
	//		string tempPartType = incData[indexIter][5];
	//		incData[indexIter][5] = incData[indexIter][6];
	//		incData[indexIter][6] = tempPartType;
	//	}
		if (incData[indexIter].size() > 0) createRobotPart(incData[indexIter], atoi(incData[indexIter][1].c_str()));
	}
}

void RobotShop::parseModelData(vector<vector<string>> incData){
	for (int indexIter = 0; indexIter < ((int)incData.size()); indexIter++)
		createRobotModel(incData[indexIter]);
}


void RobotShop::readData(string incFileName) {
	ifstream inputFile;

	try {
		//inputFile.exceptions(ofstream::eofbit | ofstream::failbit | ofstream::badbit);
		inputFile.open(incFileName, ios::in);
	}
	catch (std::exception const& e) {}
	UserInterface tempInterface = UserInterface();
	vector<vector<string>> incData = tempInterface.readFileData(inputFile);
	if (incFileName.substr(0, 5) == "Parts")
		parsePartData(incData);
	if (incFileName.substr(0, 5) == "Model")
		parseModelData(incData);
}

vector<vector<string>> RobotShop::runReport(int reportNum) {
	switch (reportNum) {
	case(9000):return AssembleData(robotInventory, 's');
	case(9001):return AssembleData(partInventory, 10, 1, 's');
	;
	default: throw exception("No report type chosen.");
	}
};