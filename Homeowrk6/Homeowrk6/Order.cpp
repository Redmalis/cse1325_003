#include "stdafx.h"
#include "Order.h"
#include "People.h"
#include "Robot.h"

int Order::getOrderNumber() {
	return orderNumber;
}

void Order::setOrderNumber(int incOrderNumber) {
	orderNumber = incOrderNumber;
}

double Order::getRobotPrice() {
	return robot->getRobotPrice();
}

double Order::calculateShippingCost() {
	return robot->getRobotWeight();
}

double Order::calculateOrderTax(double incOrderPreTotal) {
	return getRobotPrice()*.08;
}

double Order::setTotalOrderPrice(double incPriceSum) {
	return getRobotPrice() + calculateShippingCost() + calculateShippingCost();
}

void Order::setCustomer(Customer* incCustomer) {
	customer = incCustomer;
}

Customer* Order::getCustomer() {
	return customer;
}

void Order::setSalesAssociate(SalesAssociate* incAssociate) {
	salesAssociate = incAssociate;
}

SalesAssociate* Order::getSalesAssociate() {
	return salesAssociate;
}

void Order::setRobotModel(RobotModel* incRobot) {
	robot = incRobot;
}

RobotModel* Order::getRobotModel() {
	return robot;
}

void Order::setStatus(int incStatus) {
	orderStatus = static_cast<Status>(incStatus);
}

int Order::getStatus() {
	return static_cast<int>(orderStatus);
}