#ifndef __Date_H
#define __Date_H 2016


class Date {
public:
	void setDate(int incMonth, int incDay, int incYear);
	bool valid_date(int month, int day, int year);

	string getDateStr();
	int getMonth();
	int getDay();
	int getYear();
private:
	int month;
	int day;
	int year;

};

#endif