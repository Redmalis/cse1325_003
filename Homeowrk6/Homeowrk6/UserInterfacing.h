#ifndef __UserInterfacing_H
#define __UserInterfacing_H 2016


class UserInterface {
public:
	int showMenu(int menuType);
	//int showReport(int reportType);
	void UserInterface::showReport(ofstream& incStream, vector<vector<string>> incReportData);
	vector<vector<string>> UserInterface::readFileData(ifstream& incStream);
	
	vector<string> menu_2221();
	vector<string> menu_2222();
	vector<string> menu_2223();
	vector<string> menu_2224();
	vector<string> menu_2225();
	vector<string> menu_2230();
	int menu_2231(vector<string> incQuickDisplay, int incHasTorso);
	int menu_2232(vector<vector<string>> incPartInfo);
	double menu_2233(double incPartCost);

private:
	void clearScrn();
	int menu_0();
	int menu_1();
	int menu_2();
	int menu_21();
	int menu_22();
	int menu_23();
	int menu_222();
	//int menu_22311();
	
	vector<string> menu_2220();
	int menuInput(int callingMenu, int optionNum);
};

#endif