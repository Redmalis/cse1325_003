#ifdef _WIN32
#include <Windows.h>
#endif
#include "stdafx.h"
#include "UserInterfacing.h"
#include <iostream>

int UserInterface::showMenu(int menuType) {
	while (menuType > -1 && menuType<1000) {
		clearScrn();
		cout << "Robot Shop Menu\n";
		switch (menuType) {
			case(0): {menuType = menu_0(); break; }
			case(1): {menuType = menu_1(); break; }
			case(2): {menuType = menu_2(); break; }
			case(21): {menuType = menu_21(); break; }
			case(22): {menuType = menu_22(); break; }
			case(23): {menuType = menu_23(); break; }
			case(222): {menuType = menu_222(); break; }
			case(11):
			case(211):
			case(221): {menuType = 9000+menuType; break; }
			case(223): {menuType = 2230; break; }
			default:break;
		}
	}
	return menuType;
}

void UserInterface::clearScrn() {
#if defined(_WIN32) || defined(_WIN64)
	system("cls");
#endif
#if defined(_linux)
	system("clear");
#endif
//for (int lines = 0; lines < 23; lines++) cout << "\n";
}

int UserInterface::menuInput(int callingMenu, int optionNum) {
	int choice = 0;
	int exitMenu = 0;
	int extras = 1;
	if (callingMenu > 0) {
		cout << std::to_string(optionNum + 1)<<". Go back to previous menu.\n";
		cout << std::to_string(optionNum + 2) <<". Return to Main Menu.\n";
		extras = 3;
	};
	cout << std::to_string(optionNum + extras) << ". Leave the shop.\n";
	

	while (choice < 1 || exitMenu < 1) {
		cout << "\nPlease type the number of your selection and press <enter>: ";
		cin >> choice;
		cin.ignore();
		if (choice > 0 && choice <= optionNum + extras) {
			if (choice <= optionNum) return ((callingMenu * 10) + choice);
			else if (extras == 1) return -1;
			else {
				if (choice == optionNum + 1) return (callingMenu-(callingMenu%10))/10;
				if (choice == optionNum + 2) return 0;
				if (choice == optionNum + 3) return -1;
			}
		}
		else
		{
			cout << "This is an invlid menu option.\n";
			choice = 0;
		}
	}
	return callingMenu;
}			


int UserInterface::menu_0() {
	cout << "Main Menu\n\n\n";
	cout << "1. Customer Menu.\n";
	cout << "2. Employee Menu.\n";
	
	return menuInput(0, 2);
}

int UserInterface::menu_1() {
	cout << "Customer Menu\n\n\n";
	cout << "1. View Robot Model Catalog.\n";
	cout << "2. View All My Orders.\n";
	cout << "3. view My Outstanding Orders.\n";
	return menuInput(1, 3);
}

int UserInterface::menu_2(){
	cout << "Employee Menu\n\n\n";
	cout << "1. Sales Associate Menu.\n";
	cout << "2. Product Manager Menu.\n";
	cout << "3. Pointy-Haired Boss Menu.\n";
	return menuInput(2, 3);
}

int UserInterface::menu_21() {
	cout << "Sales Associate Menu\n\n\n";
	cout << "1. View Robot Model Catalog.\n";
	cout << "2. Create a Customer Order.\n";
	cout << "3. Create a Customer Entry.\n";
	cout << "4. Create Bill of Sale.\n";
	cout << "5. View Your Sales Report.\n";
	return menuInput(21, 5);
}

int UserInterface::menu_22() {
	cout << "Product Manager Menu\n\n\n";
	cout << "1. View Robot Model Catalog.\n";
	cout << "2. Create Robot Part.\n";
	cout << "3. Create Robot Model.\n";
	return menuInput(22, 3);
}

int UserInterface::menu_23() {
	cout << "PHB Menu\n\n\n";
	cout << "1. Show Sales Report by Sales Associate.\n";
	cout << "2. Show Sales Report by Robot Model.\n";
	return menuInput(23, 2);
}

int UserInterface::menu_222() {
	cout << "Robot Part Creation Menu 1\n\n\n";
	cout << "1. Head.\n";
	cout << "2. Arm.\n";
	cout << "3. Torso.\n";
	cout << "4. Battery.\n";
	cout << "5. Locomotor.\n";
	return menuInput(222, 5);
}


vector<string> UserInterface::menu_2220() {
	vector<string> partInformation;
	string inputString = "";
	
	cout << "Enter the part name: ";
	getline(cin, inputString);
	partInformation.push_back(inputString);
	partInformation.push_back("0");
	cout << "Enter the part description: ";
	getline(cin, inputString);
	partInformation.push_back(inputString);
	cout << "Enter the part weight: ";
	getline(cin, inputString);
	partInformation.push_back(inputString);
	cout << "Enter the part cost: ";
	getline(cin, inputString);
	partInformation.push_back(inputString);

	return partInformation;
}

vector<string> UserInterface::menu_2221() {
	vector<string> partInformation;
	string inputString="";
	clearScrn();
	cout << "Robot Shop Menu\n";
	cout << "Robot Part-Head Creation Menu\n\n\n";
	partInformation = menu_2220();
	return partInformation;
}

vector<string> UserInterface::menu_2222() {
	vector<string> partInformation;
	string inputString = "";
	clearScrn();
	cout << "Robot Shop Menu\n";
	cout << "Robot Part-Arm Creation Menu\n\n\n";
	partInformation = menu_2220();
	cout << "Enter the power consumption of the arm when in use (watts): ";
	getline(cin, inputString);
	partInformation.push_back(inputString);
	return partInformation;
}

vector<string> UserInterface::menu_2223() {
	vector<string> partInformation;
	string inputString = "";
	clearScrn();
	cout << "Robot Shop Menu\n";
	cout << "Robot Part-Torso Creation Menu\n\n\n";
	partInformation = menu_2220();
	int testNum = 4;
	while (testNum < 0 || testNum>3) {
		cout << "Enter the maximum number of batter compartments for this torso (up to 3): ";
		getline(cin, inputString);
		testNum = atoi(inputString.c_str());
		if (testNum > 0 && testNum < 4) {
			partInformation.push_back(inputString);
		}
		else cout << "The number of compartments must be from 1 to 3.\n";
	}
	return partInformation;
}

vector<string> UserInterface::menu_2224() {
	vector<string> partInformation;
	string inputString = "";
	clearScrn();
	cout << "Robot Shop Menu\n";
	cout << "Robot Part-Battery Creation Menu\n\n\n";
	partInformation = menu_2220();
	cout << "Enter the maximum kilowatt hours for this battery: ";
	getline(cin, inputString);
	partInformation.push_back(inputString);
	return partInformation;
}

vector<string> UserInterface::menu_2225() {
	vector<string> partInformation;
	string inputString = "";
	clearScrn();
	cout << "Robot Shop Menu\n";
	cout << "Robot Part-Locomotor Creation Menu\n\n\n";
	partInformation = menu_2220();
	cout << "Enter the maximum speed in miles per hour for this locomotor (mph): ";
	getline(cin, inputString);
	partInformation.push_back(inputString);
	return partInformation;
}

vector<string> UserInterface::menu_2230() {
	vector<string> modelData;
	string inputString = "";
	clearScrn();
	cout << "Robot Shop Menu\n";
	cout << "Robot Model Creation Menu 1\n\n\n";
	cout << "Enter the name of the new robot model: ";
	getline(cin, inputString);
	modelData.push_back(inputString);
	modelData.push_back("0");
	
	return modelData;

}

int UserInterface::menu_2231(vector<string> incQuickDisplay, int incHasTorso) {
	
	int selectPartType = 0;
	string inputString;
	
	clearScrn();
	
	cout << "Robot Shop Menu\n";
	cout << "Robot Model Creation Menu 1\n\n\n";
	for (int vectorIndex = 0; vectorIndex < ((int)incQuickDisplay.size()); vectorIndex++) cout << incQuickDisplay[vectorIndex] + "\n";
	cout << "\n1. Head.\n";
	cout << "2. Arm.\n";
	cout << "3. Torso.\n";
	if (incHasTorso == 1) {
		cout << "4. Battery.\n";
	}
		cout << "5. Locomotor.\n";
		cout << "6. Quit adding parts finalize model.\n";
	cout << "Enter the number of the type of part you wish to add [select option 6 to stop adding parts] <enter>.\n";
	getline(cin, inputString);

	return(atoi(inputString.c_str()));
}

int UserInterface::menu_2232(vector<vector<string>> incPartInfo) {
	clearScrn();
	int indexIterOuter = 0;
	while (indexIterOuter<(int)incPartInfo.size()) {
		int indexIterInner = 0;
		cout << to_string(indexIterOuter+1) + ". ";
		while (indexIterInner < (int)incPartInfo[indexIterOuter].size()) {
			if (indexIterInner > 0) cout << " | ";
			cout << incPartInfo[indexIterOuter][indexIterInner];
			indexIterInner++;
		}
		cout << "\n";
		indexIterOuter++;
	}
	cout << "\nSelect your part from the list above.\nType the index number to the left of your part and press <enter> to add your part.";
	int partInput;
	cin >> partInput;
	return partInput-1;
}

double UserInterface::menu_2233(double incPartsCost) {
	clearScrn();
	cout << "The total cost of parts in your model is " + to_string(incPartsCost) + ".\n";
	cout << "Enter the markup you wish to assign to this model of robot and press<enter>: ";
	double tempMarkup;
	cin >> tempMarkup;
	return tempMarkup;
}
//int UserInterface::showReport(int reportType) {
	//in progress
//	return 0;
//}

void UserInterface::showReport(ofstream& incStream, vector<vector<string>> incReportData) {
	streambuf * coutbuf = NULL;
	if (incStream.is_open()) {
		coutbuf = std::cout.rdbuf(); //save old buf
		std::cout.rdbuf(incStream.rdbuf());
	}
	for (int outsideIndexer = 0; outsideIndexer < (int)incReportData.size(); outsideIndexer++)
		for (int insideIndexer = 0; insideIndexer < (int)incReportData[outsideIndexer].size(); insideIndexer++)
			cout << incReportData[outsideIndexer][insideIndexer]<<endl;

	if (incStream.is_open()) {
		cout.rdbuf(coutbuf);
	}
	else {
		string tempChar;
		cout << "\n" << "Press <enter> to return to previous menu.";
		//cin.ignore();
		getline(cin,tempChar);
	}
}

vector<vector<string>> UserInterface::readFileData(ifstream& incStream){
	vector<vector<string>> incDataVectors;
	if (incStream.is_open()) {
		while (!incStream.eof()) {
			try {
				string inputString;
				vector<string> tempDataVector;
				getline(incStream, inputString);
				istringstream inputStream(inputString);
				while (inputStream) {
					string tempField;
					if (!getline(inputStream, tempField, '|')) break;
					tempDataVector.push_back(tempField);
				}
				if (tempDataVector.size() > 0) incDataVectors.push_back(tempDataVector);
			}
		catch (exception &e) {}
		}
	}
	return incDataVectors;
}

