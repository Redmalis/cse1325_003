#include "stdafx.h"
#include "Date.h"


void Date::setDate(int incMonth, int incDay, int incYear) {
//	if (valid_date(incMonth, incDay, incYear)) {
//		month = incMonth;
//		day = incDay;
//		year = incYear;
//	}
//	else { throw exception("Invalid date specified."); }
}


/*
bool Date::valid_date(int month, int day, int year) {
	if (year > 0 && year < 10000) {
		switch (month) {
			case (1, 3, 5, 7, 8, 10, 12): {if (day > 0 && day < 32)  return 1; }
			case (3, 6, 9, 11): {if (day > 0 && day < 31)  return 1; }
			case 2: {if ((day > 0 && day < 29) || ((day > 0 && day < 30) && (year % 4 == 0)))  return 1; }
			default: { return 0; }
		}
	}
	else return 0;
}
*/
string Date::getDateStr(){
	string tempDate= std::to_string(month) + "/" + std::to_string(day) + "/" + std::to_string(year);
	return tempDate;
}

int Date::getMonth() {
	return month;
}

int Date::getDay() {
	return day;
}

int Date::getYear() {
	return year;
}